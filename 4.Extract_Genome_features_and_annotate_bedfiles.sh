#!usr/bin/bash

# Code by Tom Giles (The Advanced Data Analysis Center, Nottingham, UK)
# Bedtools version 2.27.1

#################################################################
# Generate Genome resources files
#################################################################

Generate_genome_resources(){

# REPEATMASKER from bed to bed (Plus convert UCSC to Ensembl)
# ensure there is a genome features folder
mkdir ./Genome/Genome_Features

#convert UCSC repeatmasker to Ensemble bed
#UCSC_2_ENSEMBL is from https://github.com/dpryan79/ChromosomeMappings

awk 'BEGIN{IFS="\t"}{OFS="\t"}{if(NR>3) {if($9=="C"){strand="-"}else{strand="+"};print $5,$6-1,$7,$10,".",strand,$11}}' './Genome/hg38.fa.out' > './Genome/RepeatMask.bed'
awk 'BEGIN{IFS="\t"}{OFS="\t"}; NR == FNR{a[$1] = $2;next}; { if ($1 in a) {print a[$1],$2,$3,$4,$5,$6,$7} }' './Genome/UCSC_2_ENSEMBL.tsv' './Genome/RepeatMask.bed'  > './Genome/RepeatMask_ensembl.bed'


featuretypes=$(cat ./Genome/RepeatMask_ensembl.bed | cut -f 7 | sort | uniq | tr '/' '=')

for feature in $featuretypes;
 do
 full_feature_Id=$(echo "$feature" | tr '=' '/')
 awk -v var="$full_feature_Id" 'BEGIN{IFS="\t"}{OFS="\t"} {if (match($7, var)) {print $1,$2+1,$3+1,$4,$5,$6,$5,"repeat_masked_region=" $7}}' './Genome/RepeatMask_ensembl.bed'
done > ./Genome/Genome_Features/repeatmasker.bed

# GENE REGULATION From GTF to Bed
#get feature types
featuretypes=$(cat './Genome/homo_sapiens.GRCh38.Regulatory_Build.regulatory_features.20161111.gff' | cut -d';' -f 5 | sort | uniq | tr ' ' '-')

for feature in $featuretypes;
 do
 full_feature_Id=$(echo $feature | tr '-' ' ')
 filename=$(echo ${feature#$'feature_type='})
 awk -v var="$filename" -v var2="$full_feature_Id" 'BEGIN {IFS="\t|;"}{OFS="\t"}; {if (match($12, var2)) {print $1,$4,$5,substr($9, 4, 15),$6,$7,$8,"regulatory_feature=" var}}' ./Genome/homo_sapiens.GRCh38.Regulatory_Build.regulatory_features.20161111.gff
done > ./Genome/Genome_Features/regulatory_features.bed

# GENE BIOTYPES - From GTF to bed
#get feature types
featuretypes=$( awk '{if (match($3, "gene")) {print $0}}' './Genome/Homo_sapiens.GRCh38.90.gtf' | cut -d';' -f 5 | sort | uniq | tr ' ' '-')
awk 'BEGIN {FS="\t";OFS="\t"}; {if (match($3, "gene")) {print $0}}' './Genome/Homo_sapiens.GRCh38.90.gtf' > './Genome/genes.gtf'

for feature in $featuretypes; 
 do 
 full_feature_Id=$(echo $feature | tr '-' ' '); 
 filename=$(echo ${feature#$'-gene_biotype-'} | tr -d '"'); 
 awk -v var="$filename" -v var2="$full_feature_Id" 'BEGIN {FS="\t|;";OFS="\t"}; /^#/ {next};  {if (match($13, var2)); {print $1,$4,$5,substr($9, 10, 15),$6,$7,$8,"gene_biotype=" var}}' '.y/Genome/genes.gtf'
done > ./Genome_Features/biotypes.bed;

# GENE FEATURES - From GTF to bed

featuretypes=$( cat '/home/svztg/Desktop/Homo_sapiens.GRCh38.90.gtf' |  grep -v '^#' | cut -f 3 | sort | uniq | grep -v "gene" | grep -v "Selenocysteine")

for feature in $featuretypes;
 do
 awk -v var="$feature" 'BEGIN {FS="\t";OFS="\t"}; {if (match($3, var)) {print $1,$4,$5,substr($9, 10, 15),$6,".",$8,"gene_feature=" var}}' "./Genome/Homo_sapiens.GRCh38.90.gtf";

done | sort -n | uniq > ./Genome/Genome_Features/features.bed

awk -v var='-' 'BEGIN {FS="\t|;"}{OFS="\t"}; {if (match ($7, var)) {print $1,$5+3000,$5+1,substr($9, 10, 15),$6,$7,$8,"gene_feature=3kb_upstream"} else {print $1,$4-3000,$4-1,substr($9, 10, 15),$6,$7,$8,"gene_feature=3kb_upstream"}}' './Genome/Genome_Features/genes.gtf' >> ./Genome/Genome_Features/features.bed
awk -v var='-' 'BEGIN {FS="\t|;"}{OFS="\t"}; {if (match ($7, var)) {print $1,$4-3000,$4-1,substr($9, 10, 15),$6,$7,$8,"gene_feature=3kb_downstream"} else {print $1,$5+3000,$5+1,substr($9, 10, 15),$6,$7,$8,"gene_feature=3kb_downstream"}}' './Genom/Genome_Features/genes.gtf' >> ./Genome/features.bed

cat ./Genome/Genome_Features/*.bed | sort -n | uniq > ./Genome/allfeatures.sorted.bed
}

##########################################################
# ANNOTATE CONSENSUS FILES 
##########################################################

annotate_conseusus(){

#generate Intesection
for file in *consensus.bed; 
 do 
  bedtools intersect -wo -a $file -b ./Genome/allfeatures.sorted.bed > ${file%.bed}.annotated.bed; 
  done

#get features
features=$(cat *consensus.annotated.bed | cut -f 13 | sort | uniq);

#count features
for file in *.consensus.annotated.bed; 
 do echo $file > ${file%.bed}.counts; 
 for feature in $features; 
   do count=$(awk -v var="$feature" 'BEGIN {IFS="\t|;"}{OFS="\t"}; {if (match($13, var)) {print $0}}' $file | wc -l); echo $count; 
 done >> ${file%.bed}.counts; 
done;

#write results
echo "FEATURES" > consensus.all_features.txt; 

for feature in $features; 
 do 
 echo "$feature"; 
done >> consensus.all_features.txt

paste consensus.all_features.txt *.counts > consensus.annotated.counts.matrix.tsv
}
##########################################################
# ANNOTATE DE FILES 
##########################################################

annotate_DE(){

#generate Intesection
for file in *DE.bed; 
 do 
 tail -n +2 $file | bedtools intersect -wo -a - -b /home/svztg/Projects/ADAC/ADAC1075-Human-DNA-modifications-6mA/2nd_try/Genome/allfeatures.sorted.bed > ${file%.bed}.annotated.bed; 
done

#get features
features=$(cat *DE.annotated.bed | cut -f 17 | sort | uniq);

#count features
for file in *.DE.annotated.bed; 
 do 
 echo $file > ${file%.bed}.counts; 
 for feature in $features; 
  do 
  count=$(awk -v var="$feature" 'BEGIN {IFS="\t|;"}{OFS="\t"}; {if (match($17, var)) {print $0}}' $file | wc -l); echo $count; 
 done >> ${file%.bed}.counts; 
done;

#write results
echo "FEATURES" > DE.all_features.txt; 
for feature in $features; 
 do 
 echo "$feature"; 
done >>DE. all_features.txt
paste DE.all_features.txt *.counts > DE.annotated.counts.matrix.tsv
}

##########################################################
# GET FEATURE COVERAGE MATRIX FOR CONSENSUS FILES 

##########################################################

consensus_coverage_matrix(){

features=$(cat *consensus.annotated.bed | cut -f 13 | sort | uniq)

for file in *conseunsus.annotated.bed;
	do
	echo $file > ${file%.bed}.feature_coverage_in_bp.tsv
	
	for feature in $features; 
		do 
		totallenght=$(cat $file | awk -v var="$feature" 'BEGIN {IFS="\t|;"}{OFS="\t"}; {if (match($13, var)) {sum+=($14)}} END {if(sum == "") {print "0"} else {print sum}}'); 
		echo $totallenght; 
	done >> ${file%.bed}.feature_coverage_in_bp.tsv
done

echo "FEATURES" > consensus.all_features.txt; for feature in $features; do echo "$feature"; done >> consensus.all_features.txt
paste consensus.all_features.txt *conseunsus.annotated.feature_coverage_in_bp.tsv > consensus.feature_coverage.matrix.tsv
}
##########################################################
# GET FEATURE COVERAGE MATRIX FOR DE FILES 
##########################################################

DE_coverage_matrix(){

features=$(cat *DE.annotated.bed | cut -f 17 | sort | uniq)

for file in *.bed;
	do
	echo $file > ${file%.bed}.feature_coverage_in_bp.tsv
	
	for feature in $features; 
		do 
		totallenght=$(cat $file | awk -v var="$feature" 'BEGIN {IFS="\t|;"}{OFS="\t"}; {if (match($17, var)) {sum+=($18)}} END {if(sum == "") {print "0"} else {print sum}}'); 
		echo $totallenght; 
	done >> ${file%.bed}.feature_coverage_in_bp.tsv
done

echo "FEATURES" > DE.all_features.txt; for feature in $features; do echo "$feature"; done >> DE.all_features.txt
paste DE.all_features.txt *DE.annotated.feature_coverage_in_bp.tsv > DE.feature_coverage.matrix.tsv

}
##########################################################
# EXTRACT SATALLITE COUNTS FROM CONSENSUS FILES 
##########################################################

extract_satallite_counts(){

for file in *consensus.annotated.bed; 
	do 
	echo "$file" > ${file%.bed}.satillite.counts; 

	for feature in $(cat *.bed | grep "Satellite" | cut -f 9 | sort -u); 
		do 
		count=$(grep $feature $file | wc -l); 
		echo $feature $count; 
	done >> ${file%.bed}.satillite.counts; 
done;
}

##########################################################
# RUN FUNCTIONS
##########################################################

Generate_genome_resources
annotate_conseusus
annotate_DE
consensus_coverage_matrix
DE_coverage_matrix
extract_satallite_counts