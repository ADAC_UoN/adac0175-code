#!usr/bin/bash 

# Code by Tom Giles (The Advanced Data Analysis Center, Nottingham, UK)

# Skewer version 0.2.2
# BWA version 0.7.15
# Samtools version 1.3.1
# Picard Tools version 2.5.0
# PhantomPeakQualTools version - Feb 13, 2012
# MACS2 version 2.1.1

# This code runs on files in the current working directory

#global variables
GENOME_SEQUENCE="../Homo_sapiens.GRCh38.dna.toplevel_Ens89.fa"

#build the genome
buildgenome(){
	bwa index -a bwtsw ../Homo_sapiens.GRCh38.dna.toplevel_Ens89.fa;
    samtools faidx ../Homo_sapiens.GRCh38.dna.toplevel_Ens89.fa;
	echo "build Genome";
}

#trim reads, map to genome, add read group and sort
trimandmap(){
for fastq1 in *_R1_001.fastq.gz
	do
	fastq2=${fastq1%_R1_001.fastq.gz}_R2_001.fastq.gz
	skewer_out=${fastq1%_R1_001.fastq.gz}
	skewer1=${fastq1%_R1_001.fastq.gz}-trimmed-pair1.fastq.gz
	skewer2=${fastq1%_R1_001.fastq.gz}-trimmed-pair2.fastq.gz
	samfile=${fastq1%_R1_001.fastq.gz}.sam
	bamfile_with_readgroup=${samfile%.sam}.sorted.readgrp.bam;
        RGSM=${samfile%.sam}
        RGID=$(echo "$samfile" | cut -d"_" -f3 | tail -c 2);
        RGPU=$(echo "unit""$RGID");

	echo "trimming Reads"
	skewer -t 8 -q 20 -z -o $skewer_out $fastq1 $fastq2		

	echo "mapping $skewer1 $skewer2 sorting and adding read group"
	bwa mem -t 8 ../Homo_sapiens.GRCh38.dna.toplevel_Ens89.fa $skewer1 $skewer2 | \
        samtools sort -@ 8 - | \
        picard-tools AddOrReplaceReadGroups \
        I=/dev/stdin \
        O=$bamfile_with_readgroup \
        RGID=$RGID \
        RGLB=lib1 \
        RGPL=illumina \
        RGPU=$RGPU \
        RGSM=$RGSM \
        VALIDATION_STRINGENCY=LENIENT;

done
}

# Deduplicate and Merge files
dedupandmerge(){
for prefix in $(ls  *.sorted.readgrp.bam | cut -d"_" -f1 | sort | uniq);
        do
	filelist=$(find ./ -name "$prefix*.bam" | sort)
        metrics=$prefix.dedup.metrics
        merged_bamfile=$prefix.merged.bam
        merged_dedupicated_bamfile=$prefix.merged.dedup.bam


        samtools merge -@ 8 $merged_bamfile $filelist

        picard-tools MarkDuplicates \
                            METRICS_FILE=$metrics \
                            REMOVE_DUPLICATES=true \
                            ASSUME_SORTED=true  \
                            INPUT=$prefix.merged.bam \
                            OUTPUT=$merged_dedupicated_bamfile;
done
}

# Check the quality and call peaks
callpeaks(){
for merged_dedupicated_bamfile in *.merged.dedup.bam;
        do
	phantompeakqualtools=${merged_dedupicated_bamfile%.merged.dedup.bam}.phantompeakqualtools.tsv
        Input="Input.merged.dedup.bam"
        Name_narrow=${merged_dedupicated_bamfile%.merged.dedup.bam}.narrow
        Name_broard=${merged_dedupicated_bamfile%.merged.dedup.bam}.broard

        echo "Merging files into $merged_bamfile and deduplicating"

        if [ $merged_dedupicated_bamfile = "Input.merged.dedup.bam" ];
                then
                echo "this is the input file only generating stats"

                run_spp_nodups -c=$merged_dedupicated_bamfile -savp -out=$phantompeakqualtools
        else
            	echo "Generating peaks for $merged_dedupicated_bamfile"

                run_spp_nodups -c=$merged_dedupicated_bamfile -savp -out=$phantompeakqualtools & macs2 callpeak -t $merged_dedupicated_bamfile -c $Input -f BAMPE -n $Name_broard --broad -g hs --broad-cutoff 0.1 & macs2 callpeak -t $merged_dedupicated_bamfile -c $Input -f BAMPE -g hs -n $Name_narrow -B -q 0.01
        fi
done
}

buildgenome
trimandmap
sortsam
dedupandmerge
callpeaks

