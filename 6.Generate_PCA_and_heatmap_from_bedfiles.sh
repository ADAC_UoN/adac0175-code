#!usr/bin/bash
# Script by Tom Giles - Advanced Data Analysis Center
# Adapted from https://github.com/arq5x/tutorials/blob/master/bedtools.md
# Bedtools version 2.27.1


for folder in comparison*;

	do 
	cd $folder;


	parallel "bedtools jaccard -a {1} -b {2} | awk 'NR>1' | cut -f 3 > {1}.{2}.jaccard" ::: `ls *.bed` ::: `ls *.bed`

	find . | grep jaccard | xargs grep "" | sed -e s"/\.\///" | perl -pi -e "s/.bed./.bed\t/" | perl -pi -e "s/.jaccard:/\t/" > pairwise.dnase.txt

	cat pairwise.dnase.txt | sed -e 's/.hotspot.twopass.fdr0.05.merge.bed//g' | sed -e 's/.hg19//g' > pairwise.dnase.shortnames.txt

	awk 'NF==3' pairwise.dnase.shortnames.txt | python ../8a.make-matrix.py > dnase.shortnames.distance.matrix

	cut -f 1 dnase.shortnames.distance.matrix | cut -f 1 -d "-" | cut -f 1 -d "_" > labels.txt

	Rscript ../8b.PCA.and.heatmap.R
	
	rm *.jaccard;
	cd ..
done
