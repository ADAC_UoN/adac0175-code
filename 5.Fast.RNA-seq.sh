#!usr/bin/bash

# FAST TRIMMING, ALIGNMENTS AND QUATIFICATION OF TRANSCRIPT EXPRESSION WITH SALMON
# Created by: Tom Giles (The Advanced Data Analysis Center
# Date: 21st Dec 2017
# Version 0.1.1

# This script runs skewer salmon and samtools to generate very fast counts per transcript
# direct from FQ files. It assumes that the reads are formated *.R1.fastq.gz / *.R1.fastq.gz
# if your reads are formated differently please contact me or adjust the script below.

##########################################################################################
#SET USER VARIABLES
##########################################################################################

#!usr/bin/bash
INDEX_NAME='./Genome/Homo_sapiens.GRCh38.hisat2'
WORKING_DIRECTORY='.'
READ1_SUFFIX=.R1.fastq.gz
READ2_SUFFIX=.R2.fastq.gz
GTF='./Genome/Homo_sapiens.GRCh38.90.gtf'               

hisat2-build ../Genome/Homo_sapiens.GRCh38.dna.toplevel.fa ../Genome/Homo_sapiens.GRCh38.hisat2

for fq1 in $WORKING_DIRECTORY/*$READ1_SUFFIX;

       do
	# set local variables
	filename=${fq1%$READ1_SUFFIX} 
       	fq2=${filename}$READ2_SUFFIX
       	fqt1=${filename}.R1.fastq-trimmed-pair1.fastq
       	fqt2=${filename}.R1.fastq-trimmed-pair2.fastq
       	fqt1c=${filename}_R1.trimmed.fastq
       	fqt2c=${filename}_R2.trimmed.fastq
	samfile=$filename.sam
	mapping_statistics=$filename.mapping.stats
	unaligned_singletons=$filename.unmapped.single.fastq
	unaligned_pairs=$filename.unmapped.pair.fastq
	bamfile=$filename.bam
	outgtf=$filename.gtf
	echo "Now mapping $filename";

	#trim file with skewer
	if [ -f $fqt1c ] && [ -f $fqt2c ]; then
               	echo "reads are trimmed"
        else
	      	echo "trimming with skewer"
               skewer -t 16 -l 31 -Q 20 -q 3 $fq1 $fq2;
              	mv $fqt1 $fqt1c;
         	mv $fqt2 $fqt2c;
        fi
	#
	if [ -f $bamfile ]; then
                echo "reads are mapped"
        else
	hisat2 -x $INDEX_NAME -p 8 -1 $fqt1c -2 $fqt2c --met-file $mapping_statistics --un-gz $unaligned_singletons --un-conc-gz $unaligned_pairs | samtools sort - -o $bamfile
	fi
	
	if [ -f $outgtf ]; then
                echo "assembly exists"
        else
	stringtie $bamfile -G $GTF  -p 8 -o $outgtf;
	fi
done;

find $WORKING_DIRECTORY/ -maxdepth 3 -name "*.gtf">$WORKING_DIRECTORY/list_of_GTF_files.txt;

stringtie --merge -G $GTF  -o $WORKING_DIRECTORY/Merged.gtf $WORKING_DIRECTORY/list_of_GTF_files.txt

for bamfile in $WORKING_DIRECTORY/*bam; 

		do 
		results_folder="${bamfile%.bam}_ballgown_assembly"

		echo "now counting reads per transcripts for $bamfile"
		stringtie $bamfile -G $WORKING_DIRECTORY/Merged.gtf -p 8 -b $results_folder;
done