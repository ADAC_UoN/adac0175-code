# README #

## Bioinformatics Methods: ##

The outputs of the pipelines used in this analysis are also available on bitbucket (https://bitbucket.org/ADAC_UoN/adac1075-bed-files/).

## Preprocessing ##

As the files were provided annotated with their default names that came off the sequencing machine. We chose to rename them so that we could batch process them. 

*See script: 1.tidy_samples.sh*

## Identification of DMRs ##

The 150bp Illumina paired end reads were trimmed using Skewer to remove low quality sequences (1) . Reads that passed filtering were aligned to the human Ensembl genome (build hg38.89) using BWA with default parameters (2). As each biological sample was split across multiple lanes of sequencing the corresponding alignments were merged with Samtools and deduplicated (to remove PCR artifacts) with picard-tools MarkDuplicates (3,4). The impact of each pulldown was assessed using Phantompeakqualtools and highly methylated regions (HMRs) were identified using MACS2.1.1 (5,6). 
*See script:  2.ChipSeq.pipeline.sh.*

## Analysis on Consensus and Differentially Methylated regions (DMRs) ##

High confidence peaks consensus peaks and DMRs were identified using the bioconductor package DiffBind (7). Consensus peaks were defined using the dba.peakset() function to select for peaks overlapping in both replicates. DMR’s were defined using the diffbind dba.analyze() method DBA_DESEQ2 (P>0.05). In each instance the replicate sample BAM / bed files along with the corresponding input samples were used as input. The R package ChipQC was also employed in order to generate figures to asses the quality of the data (8). 

*See script:  3.Consensus_and_DE_Analysis.R*

## Annotations of bedfiles using various genome resources ##

The Ensembl Genome GTF file (Homo_sapiens.GRCh38.89.gtf) was downloaded and processed to extract gene biotypes, annotated gene features, introns, intergenic regions and sequences 3kb up/downstream of genes (9). The Repeatmasker Library (db20140131) was downloaded from UCSC, converted to the ensembl gene build and processed to identify repeat masked regions (10). The Ensembl regulatory build (HgGRCh38.Regulatory_Build.regulatory_features.20161111) was downloaded and process to identify regulatory features (11). The consensus and DMR bedfiles were annotated with this list of gene features, gene biotypes, repeatmasked sequences and regulatory regions using BedTools intesect (12). The Features present in each of the samples were then counted (using awk) and processed into a matrix. As Satellites sequences were of specific interest their counts were also processed separately (in order to isolate satellite sub-classes). 

*See script: 4.Extract_Genome_features_and_annotate_bedfiles.sh*

## Comparison with RNAseq analysis ##

RNA-seq reads from a previously published dataset (ref) were re-analysed using the hg38.89 assembly. The raw reads were trimmed using Skewer and Normalised gene expression values (RPKM) were calculated for the human transcript set (hg38.89) using Hisat2 Sringtie and Ballgown [4 / 5a.]. (13). The mean levels of expression in genes with / without peaks was (both across the entire length of genes [5b.] were compared and a series of graphs generated.

*See scripts: 5.Fast.RNA-seq.sh 5a.Ballgown_RNAseq.R 5b.RNAseq_Statistics_per_gene.R*

## Comparison with Histone methylation data ##

ChiP-seq peaks from a previously published dataset (ref) containing the peak profiles of H3k09me3, H3k27ac, H3k27me3, H3k36me3, H3k4me1, H3k4me2, H3k4me3 and H3k79me2 were compared against the consensus peaks using an adaptations of Aaron Quinlans bedtools jacard statistic pipleine (15). This method generated a series of histograms and PCA plots. 

*See scripts: 6.Generate_PCA_and_heatmap_from_bedfiles.sh / 6a.make-matrix.py / 6b.PCA.and.heatmap.R*

## References ##
```
1. Hongshan Jiang, Rong Lei, Shou-Wei Ding and Shuifang Zhu, 2014, Skewer: a fast and accurate adapter trimmer for next-generation sequencing paired-end reads, BMC Bioinformatics 15:182
2. Heng Li and Richard Durbin, 2009, Fast and accurate short read alignment with Burrows–Wheeler transform, Bioinformatics. 25(14): 1754–1760
3. Heng Li, Bob Handsaker, Alec Wysoker, Tim Fennell, Jue Ruan, Nils Homer, Gabor Marth, Goncalo Abecasis and Richard Durbin, 2009, The Sequence Alignment/Map format and SAMtools, Bioinformatics, 25:16 2078–2079
4. “Picard Toolkit.” 2018. Broad Institute, GitHub Repository. http://broadinstitute.github.io/picard/; Broad Institute
5. Georgi K. Marinov, Anshul Kundaje, Peter J. Park and Barbara J. Wold, 2014, Large-Scale Quality Analysis of Published ChIP-seq Data, G3 (Bethesda), 4(2): 209–223 
6. Yong Zhang, Tao Liu, Clifford A Meyer, Jérôme Eeckhoute, David S Johnson, Bradley E Bernstein, Chad Nusbaum, Richard M Myers, Myles Brown, Wei Li and X Shirley Liu, 2008, Model-based Analysis of ChIP-Seq (MACS), Genome Biology, 9:R137
7. Stark R., Brown G. D. (2011). DiffBind: Differential Binding Analysis of ChIP-Seq Peak Data. Bioconductor. Available online at: http://bioconductor.org/packages/release/bioc/html/DiffBind.html.
8. Carroll TS, Liang Z, Salama R, Stark R and de Santiago I (in press). “Impact of artefact removal on ChIP quality metrics in ChIP-seq and ChIP-exo data.” Frontiers in Genetics.
9. Daniel R Zerbino Premanand Achuthan Wasiu Akanni M Ridwan Amode Daniel Barrell Jyothish Bhai Konstantinos Billis Carla Cummins Astrid Gall Carlos García Girón Laurent Gil Leo Gordon Leanne Haggerty Erin Haskell Thibaut Hourlier Osagie G Izuogu Sophie H Janacek Thomas Juettemann Jimmy Kiang To Matthew R Laird Ilias Lavidas Zhicheng Liu Jane E Loveland Thomas Maurel William McLaren Benjamin Moore Jonathan Mudge Daniel N Murphy Victoria Newman Michael Nuhn Denye Ogeh Chuang Kee Ong Anne Parker Mateus Patricio Harpreet Singh Riat Helen Schuilenburg Dan Sheppard Helen Sparrow Kieron Taylor Anja Thormann Alessandro Vullo Brandon Walts Amonida Zadissa Adam Frankish Sarah E Hunt Myrto Kostadima Nicholas Langridge Fergal J Martin Matthieu Muffato Emily Perry Magali Ruffier Dan M Staines Stephen J Trevanion Bronwen L Aken Fiona Cunningham Andrew Yates Paul Flicek, 2018, Ensembl 2018, Nucleic Acids Research, 46:D1 D754–D761, 
10. Smit, AFA, Hubley, R & Green, P. RepeatMasker Open-4.0. 2013-2015 <http://www.repeatmasker.org>.
11. aniel R Zerbino, Steven P Wilder, Nathan Johnson, Thomas Juettemann and Paul R Flicek, 2015, The Ensembl Regulatory Build, Genome Biology 16:56 doi:10.1186/s13059-015-0621-5
12. Arron R Quinlan, Ira M Hall, 2010, BEDTools: a flexible suite of utilities for comparing genomic features, Bioinformatics, 26:6, 841–842
13. Mihaela Pertea, Daehwan Kim, Geo M Pertea, Jeffrey T Leek & Steven L Salzberg, 2016, Transcript-level expression analysis of RNA-seq experiments with HISAT, StringTie and Ballgown, Nature Protocols, 11, 1650–1667
14. Charlotte Soneson, Michael I. Love, Mark D. Robinson. 2015, Differential analyses for RNA-seq: transcript-level estimates improve gene-level inferences, F1000Research, 4:1521
15. https://github.com/arq5x/tutorials/blob/master/bedtools.md
```