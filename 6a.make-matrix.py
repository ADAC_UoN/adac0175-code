#!/usr/bin/python env
import sys
import collections 

matrix = collections.defaultdict(dict)
for line in sys.stdin:
	fields = line.strip().split()
	matrix[fields[0]][fields[1]] = float(fields[2])

keys = sorted(matrix.keys())