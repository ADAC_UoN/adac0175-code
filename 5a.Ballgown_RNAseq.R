## ----loadmethods, echo=FALSE, message=FALSE, warning=FALSE---------------
source("http://bioconductor.org/biocLite.R")


# Package list:
list.of.packages <- c("BiocManager","edgeR","ballgown","gtool","ggplot2","gplots","genefilter","GenomicRanges","plyr","org.Hs.eg.db","GOstats","biomaRt","pathview","gage","gageData","ReactomePA")

#  List packages that are not in the current library:
new.packages <- list.of.packages[!(list.of.packages %in% installed.packages()[,"Package"])]

# Install packages that are not in the current library:
if(length(new.packages)) BiocManager::install(new.packages)

#Load all packages:
lapply(list.of.packages, require, character.only = TRUE)

## ----Variables-----------------------------------------------------------

data_directory ='/Users/Tom/Documents/Digital_Research/Projects/ADAC1075 - Alexey - m6A DRIP'
## ----Set the working directory and import the experiemnt -----------------------------

setwd(data_directory)
directory <- getwd()

pheno_data = read.table(file ="design_matrix", header = TRUE, sep = "\t")
group = as.factor(pheno_data[,"condition"])
bg = ballgown(dataDir=data_directory, samplePattern='.salmon.quantification', meas='all', pData=pheno_data)
transcript_gene_table = indexes(bg)$t2g

## Plot the number of Transcripts per gene ---------------------------------------------

pdf("Transcripts.per.gene.pdf",width=14,height=14)
counts=table(transcript_gene_table[,"g_id"])
c_one = length(which(counts == 1))
c_more_than_one = length(which(counts > 1))
c_max = max(counts)
hist(counts, breaks=50, col="bisque4", xlab="Transcripts per gene", main="Distribution of transcript count per gene")
legend_text = c(paste("Genes with one transcript =", c_one), paste("Genes with more than one transcript =", c_more_than_one), paste("Max transcripts for single gene = ", c_max))
legend("topright", legend_text, lty=NULL)
dev.off()

## Plot the distrabution of transcript lengths  ---------------------------------------------

pdf("Distrabution.of.transcript.lengths.pdf")
full_table <- texpr(bg , 'all')
t_max = max(full_table$length)
t_mean= mean(full_table$length)
hist(full_table$length, breaks=50, xlab="Transcript length (bp)", main="Distribution of transcript lengths", col="steelblue")
legend_text = c(paste("Longest transcript =", t_max),paste("Average transcript =", t_mean))
legend("topright", legend_text, lty=NULL)
dev.off()
## Filter low abundance transcripts ---------------------------------------------

bg_filt = subset(bg,"rowVars(texpr(bg)) >1",genomesubset=TRUE) 

## Load gene names ---------------------------------------------
bg_table = texpr(bg_filt, 'all')
bg_gene_names = unique(bg_table[, 9:10])

## Gene Expression ---------------------------------------------
gene_expression = as.data.frame(gexpr(bg_filt))
shortnames=gsub("_ballgown_assembly","",gsub("FPKM.","",colnames(gene_expression)))
data_columns=c(1:ncol(gene_expression))

## Transcript Expression---------------------------------------------
transcript_expression = texpr(bg_filt, 'FPKM')
transcript_values = texpr(bg_filt, 'all')
transcript_names = as.data.frame(transcriptNames(bg_filt))
colnames(transcript_names)[1] <- "transcript_id"

## Boxplot of libaries---------------------------------------------


pdf("Boxplot.FPKM.distribtion.between.libraries.pdf",width=14,height=14)
boxplot(log2(gene_expression[,data_columns]+1), col=group, names=shortnames, las=2, ylab="log2(FPKM)", main="Distribution of FPKMs for all 6 libraries")
dev.off()

## PCA plot of gene expression values---------------------------------------------

pdf("pca.plot.pdf",width=14,height=14)
gene_expression[,"sum"]=apply(gene_expression[,data_columns], 1, sum)
d=1-cor(gene_expression[which(gene_expression[,"sum"] > 5),data_columns], use="pairwise.complete.obs", method="pearson")
mds=cmdscale(d, k=2, eig=TRUE)
par(mfrow=c(1,1))
plot(mds$points, type="n", xlab="", ylab="", main="MDS distance plot (all non-zero genes) for all libraries", xlim=c(-0.15,0.15), ylim=c(-0.15,0.15))
points(mds$points[,1], mds$points[,2], col=group, cex=2, pch=16)
text(mds$points[,1], mds$points[,2], shortnames)
dev.off()

## Extract Reference Annotations from GTF-----------------------------------------------

GFF.annotation <- gffRead("gffcmp.annotated.gtf", nrows = -1, verbose = FALSE)
GFF.annotation[,"ref_id"] = getAttributeField(GFF.annotation$attributes, field = "ref_gene_id", attrsep = "; ")
GFF.annotation[,"gene_id"] = getAttributeField(GFF.annotation$attributes, field = "gene_id", attrsep = "; ")
GFF.annotation[,"transcript_id"] = getAttributeField(GFF.annotation$attributes, field = "transcript_id", attrsep = "; ")

Reflist <- unique(GFF.annotation[,c("ref_id","gene_id","transcript_id")])
Reflist <- Reflist[complete.cases(Reflist), ]
ref2genes <- unique(Reflist[,c("ref_id","gene_id")])
ref2genes <- print(as.data.frame(sapply(ref2genes, function(x) gsub("\"", "", x))))
ref2transcripts <- unique(Reflist[,c("ref_id","transcript_id")])
ref2transcripts <- print(as.data.frame(sapply(ref2transcripts, function(x) gsub("\"", "", x))))

## Gene Expression-------------------------------------------------------------------

results_genes = stattest(bg_filt, feature="gene", covariate="condition", getFC=TRUE, meas="FPKM")
results_genes <- merge(bg_gene_names,results_genes,by.x=c("gene_id"),by.y=c("id"))
results_genes <- merge(ref2genes,results_genes,by.x=c("gene_id"),by.y=c("gene_id"),all=TRUE)
colnames(results_genes)[5] <- "Ratio"
results_genes[,"Log_FC"] = log2(results_genes$Ratio)
results_genes[,"FC"] = logratio2foldchange(results_genes$Log_FC, base=2)
results_genes <- merge(results_genes, gene_expression,by.x=c("gene_id"),by.y=0, all=T)
results_genes = results_genes[ order(results_genes$pval), ]

pdf("Distribtion.of.DE.Genes.pdf",width=14,height=14)
sig=which(results_genes$pval<0.05)
hist(results_genes[sig,"Log_FC"], breaks=50, col="seagreen", xlab="log2(Fold change)", main="Distribution of gene differential expression values")
abline(v=-1, col="black", lwd=2, lty=2)
abline(v=1, col="black", lwd=2, lty=2)
legend("topleft", "Fold-change > 2", lwd=2, lty=2)
dev.off()

## -Transcript Expression-------------------------------------------------------------

results_transcripts = stattest(bg_filt, feature='transcript', covariate="condition", getFC=TRUE, meas='FPKM')
results_transcripts <- merge(transcript_names, results_transcripts, by.x=0,by.y=c("id"))
results_transcripts <- merge(ref2transcripts, results_transcripts, by.x=c("transcript_id"),by.y=c("transcript_id"),all=TRUE)
colnames(results_transcripts)[3] <- "id"
colnames(results_transcripts)[5] <- "Ratio"
results_transcripts[,"Log_FC"] = log2(results_transcripts$Ratio)
results_transcripts[,"FC"] = logratio2foldchange(results_transcripts$Log_FC, base=2)
results_transcripts <- merge(results_transcripts, transcript_values, by.x=c("id"),by.y=0)
results_transcripts = results_transcripts[ order(results_transcripts$pval) ,]

#pdf("Expression.of.WT1.Transcripts.pdf",width = 14,height=14)
#i = which(results_transcripts[,"gene_id"] %in% "MSTRG.4088")
#results_transcripts[i,]
#plotTranscripts(gene='MSTRG.4088', gown=bg, labelTranscripts = TRUE,samples=sampleNames(bg),meas='FPKM', colorby='transcript', main='transcripts from gene WT1: FPKM')
#dev.off()

pdf("Distribution.of.DE.Transcripts.pdf",width=14,height=14)
sig=which(results_transcripts$pval<0.05)
hist(results_transcripts[sig,"Log_FC"], breaks=50, col="seagreen", xlab="log2(Fold change)", main="Distribution of transcript differential expression values")
abline(v=-1, col="black", lwd=2, lty=2)
abline(v=1, col="black", lwd=2, lty=2)
legend("topleft", "Fold-change > 2", lwd=2, lty=2)
dev.off()

pdf("MA.plot.of.Significant.Transcripts.pdf",width=14,height=14)
results_transcripts[,"Mean"] <- rowMeans(texpr(bg_filt))
ggplot(results_transcripts, aes(log2(Mean), Log_FC, colour = pval<0.05)) +
  scale_color_manual(values=c("#999999", "#FF0000")) +
  geom_point() +
  geom_hline(yintercept=0)
dev.off()

## ----writeTables--------------------------------------------------------------

write.table(pheno_data, file="Phenotype_results_table.tsv",row.names=FALSE,col.names=TRUE,quote=FALSE,sep="\t")
write.table(results_transcripts, file="Transcript_results_table.tsv",row.names=FALSE,col.names=TRUE,quote=FALSE,sep="\t")
write.table(results_genes, file="Gene_results_table.tsv",col.names = TRUE,quote=FALSE,sep="\t")

#write.table(results_transcripts[i,],file="WT1-Transcripts.tsv",col.names=TRUE,row.names=FALSE,quote=FALSE,sep='\t')


## ----Get Sig Genelist
sig_genes=which(results_genes$pval<0.05)
sig_gene_id <- results_genes[sig_genes,"gene_id"]
sig_ENSG_id <- grep("ENSG", sig_gene_id,value = T)
sig_ENSG_id <- as.data.frame(sapply(sig_ENSG_id, function(x) gsub("\"", "", x)))
goi.column = 1
myInterestingGenes <- as.vector(unlist(sig_ENSG_id[goi.column]))
 
# Set cutoff values for GO and Pathway analysis
kegg.qval.cutoff = 0.05
GO.cutoff = 0.05
min.genes.cutoff = 1

# Import the human ensembl
ensembl.spp <- "hsapiens_gene_ensembl"
species.ens.code = "Hs"
species.kegg.code = "hsa"
kegg.data.code = "hsa"
reactome.spp = "human" 
kegg.gsets.spp <- kegg.gsets(species = "hsa", id.type = "kegg")  
species.db <- paste("org",species.ens.code,"eg.db",sep=".")
ensembl = useEnsembl(biomart="ensembl", dataset=ensembl.spp)

# Generate signifcant matrix
all.genes <- getBM(attributes=c('ensembl_gene_id', 'entrezgene', 'external_gene_name'), mart = ensembl)
colnames(all.genes) <- c("ID","Entrez","Name")
all.genes.entrez <- na.omit(all.genes)
all.genes.entrez <- all.genes.entrez[all.genes.entrez$ID!="",]
goi.entrez <-unique(as.character(all.genes.entrez[all.genes.entrez$ID %in% myInterestingGenes,2]))
universe <- unique(as.character(all.genes.entrez$Entrez))


kegg.sets.test <- kegg.gsets.spp$kg.sets
kegg.sets.spp = kegg.gsets.spp$sigmet.idx

# Biological Process
params.BP <- new('GOHyperGParams',
                 geneIds=goi.entrez,
                 universeGeneIds=universe,
                 ontology='BP',
                 pvalueCutoff=GO.cutoff,
                 conditional=F,
                 testDirection='over',
                 annotation=species.db
)
hgOver.BP <- hyperGTest(params.BP)
result.BP <- summary(hgOver.BP)

if (nrow(result.BP)>=1)
{
  result.BP <- result.BP[result.BP$Count >= min.genes.cutoff,] # filter those with < cut off count
  result.BP <- result.BP[order(result.BP$Pvalue),] # order by Pvalue
  
  top.result.BP <- head(result.BP,10)
  top.result.BP$Term <- as.factor(top.result.BP$Term)
  top.result.BP$Term <- factor(top.result.BP$Term, levels = top.result.BP$Term)
  
  if (nrow(top.result.BP) > 0)
  {
    max.y.plot = 1.2*(max(-log10(top.result.BP$Pvalue)))
    sig.BP.plot <-
      ggplot(data = top.result.BP,
             aes(x = as.factor(Term), y = -log10(top.result.BP$Pvalue),
                 colour = Count,
                 scale_colour_gradient(low="blue"),
                 size = Count))+
      geom_point() +
      scale_color_continuous("GOI count")+
      scale_size_continuous(range = c(5,20), guide=FALSE)+
      geom_hline(yintercept=1.30103,lty=2, color="grey") + # equivalent of p = 0.05
      geom_hline(yintercept=2,lty=4, color="grey") + # equivalent of p = 0.01
      geom_hline(yintercept=3,lty=3, color="grey") + # equivalent of p = 0.001  
      coord_flip()+
      geom_point(stat = "identity") +
      theme_bw() +
      theme(axis.text.x = element_text(colour = "black"),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            panel.background = element_rect(fill = "white")) +
      ylim(-0.5,max.y.plot)+
      xlab("") +
      ylab("Enrichment (-log10 pvalue)")
    
    pdf("GO.BP.Significant.enrichment.plot.pdf")
    print(sig.BP.plot)
    dev.off()
  }
  
  # Add gene names to results table
  allgos.BP <- geneIdUniverse(hgOver.BP)
  output.BP.match <- NULL
  for (i in 1:nrow(result.BP))
  {
    
    go.holding = result.BP$GOBPID[i]
    all.entrez.in.GO <- as.vector(unlist(allgos.BP[go.holding]))
    goi.entrez.in.GO <- intersect(all.entrez.in.GO,goi.entrez)
    input.in.GO.IDs <- all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 1]
    input.in.GO.IDs <- unique(input.in.GO.IDs[input.in.GO.IDs != ""])
    input.in.GO.IDs <- paste(input.in.GO.IDs, collapse = " ")
    input.in.GO.external <- unique(all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 3])
    input.in.GO.external <- paste(input.in.GO.external, collapse = " ")
    temp <- cbind(go.holding,input.in.GO.IDs,input.in.GO.external)
    output.BP.match <- rbind(output.BP.match,temp)
  }
  result.BP <- merge(result.BP, output.BP.match, by.x ="GOBPID", by.y="go.holding", all.x=TRUE)
  write.table(result.BP, file="GO.BP.table.tsv", row.names = FALSE, col.names=TRUE,sep = '\t', quote=FALSE)
  
}



# Molecular Function
params.MF <- new('GOHyperGParams',
                 geneIds=goi.entrez,
                 universeGeneIds=universe,
                 ontology='MF',
                 pvalueCutoff=GO.cutoff,
                 conditional=F,
                 testDirection='over',
                 annotation=species.db
)
hgOver.MF <- hyperGTest(params.MF)
result.MF <- summary(hgOver.MF)

if (nrow(result.MF)>=1 )
{
  result.MF <- result.MF[result.MF$Count >= min.genes.cutoff,] # filter those with < cut off count
  result.MF <- result.MF[order(result.MF$Pvalue),] # order by Pvalue
  
  top.result.MF <- head(result.MF,10)
  top.result.MF$Term <- as.factor(top.result.MF$Term)
  top.result.MF$Term <- factor(top.result.MF$Term, levels = top.result.MF$Term)
  
  if (nrow(top.result.MF) > 0)
  {
    max.y.plot = 1.2*(max(-log10(top.result.MF$Pvalue)))
    sig.MF.plot <-
      ggplot(data = top.result.MF,
             aes(x = as.factor(Term), y = -log10(top.result.MF$Pvalue),
                 colour = Count,
                 scale_colour_gradient(low="blue"),
                 size = Count))+
      geom_point() +
      scale_color_continuous("GOI count")+
      scale_size_continuous(range = c(5,20), guide=FALSE)+
      geom_hline(yintercept=1.30103,lty=2, color="grey") + # equivalent of p = 0.05
      geom_hline(yintercept=2,lty=4, color="grey") + # equivalent of p = 0.01
      geom_hline(yintercept=3,lty=3, color="grey") + # equivalent of p = 0.001  
      coord_flip()+
      geom_point(stat = "identity") +
      theme_bw() +
      theme(axis.text.x = element_text(colour = "black"),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            panel.background = element_rect(fill = "white")) +
      ylim(-0.5,max.y.plot)+
      xlab("") +
      ylab("Enrichment (-log10 pvalue)")
    
    pdf("GO.MF.Significant.enrichment.plot.pdf")
    print(sig.MF.plot)
    dev.off()
  }
  
  # Add gene names to results table
  allgos.MF <- geneIdUniverse(hgOver.MF)
  output.MF.match <- NULL
  for (i in 1:nrow(result.MF))
  {
    go.holding = result.MF$GOMFID[i]
    all.entrez.in.GO <- as.vector(unlist(allgos.MF[go.holding]))
    goi.entrez.in.GO <- intersect(all.entrez.in.GO,goi.entrez)
    input.in.GO.IDs <- all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 1]
    input.in.GO.IDs <- unique(input.in.GO.IDs[input.in.GO.IDs != ""])
    input.in.GO.IDs <- paste(input.in.GO.IDs, collapse = " ")
    input.in.GO.external <- unique(all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 3])
    input.in.GO.external <- paste(input.in.GO.external, collapse = " ")
    temp <- cbind(go.holding,input.in.GO.IDs,input.in.GO.external)
    output.MF.match <- rbind(output.MF.match,temp)
  }
  result.MF <- merge(result.MF, output.MF.match, by.x ="GOMFID", by.y="go.holding", all.x=TRUE)
  write.table(result.MF, file="GO.MF.table.tsv", row.names = FALSE, col.names=TRUE,sep = '\t', quote=FALSE)
}

# Cellular Compartment
params.CC <- new('GOHyperGParams',
                 geneIds=goi.entrez,
                 universeGeneIds=universe,
                 ontology='CC',
                 pvalueCutoff=GO.cutoff,
                 conditional=F,
                 testDirection='over',
                 annotation=species.db
)
hgOver.CC <- hyperGTest(params.CC)
result.CC <- summary(hgOver.CC)

if (nrow(result.CC)>=1 )
{
  result.CC <- result.CC[result.CC$Count >= min.genes.cutoff,] # filter those with < cut off count
  result.CC <- result.CC[order(result.CC$Pvalue),] # order by Pvalue
  top.result.CC <- head(result.CC,10)
  top.result.CC$Term <- as.factor(top.result.CC$Term)
  top.result.CC$Term <- factor(top.result.CC$Term, levels = top.result.CC$Term)
  
  if (nrow(top.result.CC) > 0)
  {
    max.y.plot = 1.2*(max(-log10(top.result.CC$Pvalue)))
    sig.CC.plot <-
      ggplot(data = top.result.CC,
             aes(x = as.factor(Term), y = -log10(top.result.CC$Pvalue),
                 colour = Count,
                 scale_colour_gradient(low="blue"),
                 size = Count))+
      geom_point() +
      scale_color_continuous("GOI count")+
      scale_size_continuous(range = c(5,20), guide=FALSE)+
      geom_hline(yintercept=1.30103,lty=2, color="grey") + # equivalent of p = 0.05
      geom_hline(yintercept=2,lty=4, color="grey") + # equivalent of p = 0.01
      geom_hline(yintercept=3,lty=3, color="grey") + # equivalent of p = 0.001  
      coord_flip()+
      geom_point(stat = "identity") +
      theme_bw() +
      theme(axis.text.x = element_text(colour = "black"),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            panel.background = element_rect(fill = "white")) +
      ylim(-0.5,max.y.plot)+
      xlab("") +
      ylab("Enrichment (-log10 pvalue)")
    
    pdf("GO.CC.Significant.enrichment.plot.pdf")
    print(sig.CC.plot)
    dev.off()
  }
  
  # Add gene names to results table
  allgos.CC <- geneIdUniverse(hgOver.CC)
  output.CC.match <- NULL
  for (i in 1:nrow(result.CC))
  {
    go.holding = result.CC$GOCCID[i]
    all.entrez.in.GO <- as.vector(unlist(allgos.CC[go.holding]))
    goi.entrez.in.GO <- intersect(all.entrez.in.GO,goi.entrez)
    input.in.GO.IDs <- all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 1]
    input.in.GO.IDs <- unique(input.in.GO.IDs[input.in.GO.IDs != ""])
    input.in.GO.IDs <- paste(input.in.GO.IDs, collapse = " ")
    input.in.GO.external <- unique(all.genes[all.genes$Entrez %in% goi.entrez.in.GO, 3])
    input.in.GO.external <- paste(input.in.GO.external, collapse = " ")
    temp <- cbind(go.holding,input.in.GO.IDs,input.in.GO.external)
    output.CC.match <- rbind(output.CC.match,temp)
  }
  result.CC <- merge(result.CC, output.CC.match, by.x ="GOCCID", by.y="go.holding", all.x=TRUE)
  write.table(result.CC, file="GO.CC.table.tsv", row.names = FALSE, col.names=TRUE,sep = '\t', quote=FALSE)
}

## REACTOME PATWAY ANALYSIS

reactome.out <- enrichPathway(gene=goi.entrez,
                              pvalueCutoff=0.05,
                              readable=T,
                              organism = reactome.spp,
                              pAdjustMethod = "BH",
                              #qvalueCutoff = 0.05,
                              universe = universe
)

reactome.writeout <- (as.data.frame(reactome.out))

if (nrow(reactome.writeout) >= 1)
{
  write.table(reactome.writeout, file="reactome.pathway.enrichment.table.tsv", row.names = FALSE, col.names = TRUE, quote=FALSE, sep='\t')
  
  reactome.dot <- dotplot <- dotplot(
    reactome.out,
    showCategory=15,
    font.size = 12
  )
  tiff(filename="reactome.pathway.enrichment.dotplot.tiff",
       width = 320,
       height = 240,
       units = "mm",
       res=800
  )
  print(reactome.dot)
  dev.off()
  
  
  tiff(filename="reactome.pathway.enrichment.enrichmap.tiff",
       width = 320,
       height = 240,
       units = "mm",
       res=800,
       type = "Xlib",
       pointsize = 12
  )
  enrichMap(reactome.out,
            layout=igraph::layout.kamada.kawai,
            vertex.label.cex = 0.8
  )
  dev.off()
}

# KEGG PATHWAY ANALYSIS 

pathview.goi.entrez <- rep.int(1, length(goi.entrez))

names(pathview.goi.entrez) = goi.entrez 

keggres = gage(pathview.goi.entrez, gsets=kegg.sets.test, same.dir=TRUE) # determine kegg membership of all genes. 

keggres.pathways <- as.data.frame(keggres)
keggres.pathways.out <- keggres.pathways[keggres.pathways$greater.set.size > 0, ]

if (nrow(keggres.pathways.out) >=1)
{
  keggres.pathways.out$KEGGpathways <- rownames(keggres.pathways.out)
  matching.kegg.sets.spp <- kegg.sets.test[c(keggres.pathways.out$KEGGpathways)] # named list of matched pathways
  matching.kegg.sets.spp.total.size <- lengths(matching.kegg.sets.spp, use.names = TRUE) # named list of the number of total number of genes in matched pathway.
  
  
  matching.kegg.sets.spp.df <- as.data.frame(unlist(matching.kegg.sets.spp, use.names = TRUE))
  matching.kegg.sets.spp.df$kegg.id <-  gsub("\\d+$", "", rownames(matching.kegg.sets.spp.df)) 
  row.names(matching.kegg.sets.spp.df) <- NULL
  colnames(matching.kegg.sets.spp.df) <- c("entrez.id","kegg.id")
  
  # make subset of matching.kegg.sets.spp.df with just genes of interest in it
  goi.matching.kegg.sets.spp.df <- matching.kegg.sets.spp.df[matching.kegg.sets.spp.df$entrez.id %in% goi.entrez, ]
  
  #################################################################################################################
  # Stats details
  #################################################################################################################
  # for each pathway with > 0 goi in it, conduct a hypergeometric test using phyper
  # phyper(q, m, n, k, lower.tail = TRUE, log.p = FALSE)
  # x, q vector of quantiles representing the number of white balls drawn
  # without replacement from an urn which contains both black and white
  # balls.
  # m the number of white balls in the urn.
  # n the number of black balls in the urn.
  # k the number of balls drawn from the urn.
  # if 
  # pop size : 5260 # total number of entrez gene in all pathways
  # sample size : 131 # total goi
  # Number of items in the pop that are classified as successes : 1998 # entrez in a particular pathway
  # Number of items in the sample that are classified as successes : 62 # goi in a particular pathway
  # 
  # phyper(62,1998,5260-1998,131)
  # e.g pathway 100 genes 10 are in goi list of size 400 universe = 20,000
  # phyper(1,100,20000-100,400, lower.tail=FALSE) = 0.597 = probability of finding this many or greater goi in pathway 
  # phyper(80,100,20000-100,400, lower.tail=FALSE) = 4.603708e-122 = probability of finding this many or greater goi in pathway 
  #################################################################################################################
  #
  #################################################################################################################
  
  
  universe.size = as.numeric(length(universe))
  total.goi.size = as.numeric(length(goi.entrez))
  
  
  # do for each pathway in list and generate table of pathways passing cut off after FDR qvalue calculation
  working.pathways <- unique(matching.kegg.sets.spp.df$kegg.id)
  
  pathways.hypergeometric.results <- data.frame("Pathway"= character(0),"p.val"= numeric(0),"FDR q.val"= numeric(0),"ID"= character(0), "entrez.ids"= numeric(0), "external.ids"= character(0))
  pathways.hypergeometric.results.sig <- data.frame("Pathway"= character(0),"p.val"= numeric(0),"FDR q.val"= numeric(0), "goi.count"= numeric(0))
  
  for (i in 1:length(working.pathways))
  {
    current.pathway = working.pathways[i]
    goi.in.pathway <- as.numeric(nrow(goi.matching.kegg.sets.spp.df[goi.matching.kegg.sets.spp.df$kegg.id == current.pathway, ]))
    total.genes.in.pathway <- as.numeric(nrow(matching.kegg.sets.spp.df[matching.kegg.sets.spp.df$kegg.id == current.pathway, ]))
    
    pval <- phyper(goi.in.pathway,total.genes.in.pathway,(universe.size-total.genes.in.pathway),total.goi.size, lower.tail=FALSE)
    qval <- p.adjust(pval, method = "fdr", n = nrow(keggres.pathways.out))
    
    current.goi <- goi.matching.kegg.sets.spp.df[goi.matching.kegg.sets.spp.df$kegg.id == current.pathway, ]
    current.goi <- current.goi[1]
    current.goi.entrez.ids <- as.numeric(as.character(current.goi$entrez.id))
    
    current.goi.ens <- all.genes.entrez[all.genes.entrez$Entrez %in% current.goi.entrez.ids ,]
    
    current.goi.ens.ids <- unique(current.goi.ens$ID)
    current.goi.ext.ids <- unique(current.goi.ens$Name)
    
    
    current.goi.ens.ids <- paste(current.goi.ens.ids, collapse=", ")
    current.goi.entrez.ids <- paste(current.goi.entrez.ids, collapse=", ")
    current.goi.ext.ids <- paste(current.goi.ext.ids, collapse=", ")
    
    current.out <- as.data.frame(cbind(current.pathway,pval,qval,current.goi.ens.ids,current.goi.entrez.ids,current.goi.ext.ids))
    current.sig.out <- as.data.frame(cbind(current.pathway,pval,qval,goi.in.pathway))
    
    pathways.hypergeometric.results <- rbind(pathways.hypergeometric.results, current.out)
    
    if (qval < kegg.qval.cutoff & goi.in.pathway >= min.genes.cutoff)
    {
      pid <- substr(current.pathway, start=1, stop=8) # get kegg ids 
      pathview(gene.data=pathview.goi.entrez, pathway.id=pid, species=species.kegg.code)
      pathways.hypergeometric.results.sig <- rbind(pathways.hypergeometric.results.sig, current.sig.out)
    }
  }
  
  colnames(pathways.hypergeometric.results) <- c("Pathway","p.val","FDR q.val","Ensembl.ids","Entrez.ids","External.ids")
  
  # make FDR q.val numeric and sort 
  pathways.hypergeometric.results$`FDR q.val` <- as.numeric(as.character(pathways.hypergeometric.results$`FDR q.val`))
  pathways.hypergeometric.results <-  pathways.hypergeometric.results[with(pathways.hypergeometric.results, order(pathways.hypergeometric.results$`FDR q.val`)), ]
  
  write.table(pathways.hypergeometric.results,"KEGG.enrichment.analysis.results.table.tsv", row.names = FALSE, col.names = TRUE, quote = FALSE, sep ='\t')
  
  ##############################################################################################  
  # draw plot of enriched pathways
  ############################################################################################## 
  colnames(pathways.hypergeometric.results.sig) <- c("Pathway","p.val","FDR q.val","goi.count")
  
  if (nrow(pathways.hypergeometric.results.sig)>0)
  {
    
    
    pathways.hypergeometric.results.sig$`FDR q.val` <- as.numeric(as.character(pathways.hypergeometric.results.sig$`FDR q.val`))
    pathways.hypergeometric.results.sig <-  pathways.hypergeometric.results.sig[with(pathways.hypergeometric.results.sig, order(pathways.hypergeometric.results.sig$`FDR q.val`)), ]
    
    pathways.hypergeometric.results.sig$goi.count <- as.numeric(as.character(pathways.hypergeometric.results.sig$goi.count))
    pathways.hypergeometric.results.sig <-  pathways.hypergeometric.results.sig[with(pathways.hypergeometric.results.sig, order(-pathways.hypergeometric.results.sig$`FDR q.val`)), ]
    
    max.y.plot = 1.2*(max(-log10(pathways.hypergeometric.results.sig$`FDR q.val`)))
    
    sig.kegg.plot <-
      ggplot(data = pathways.hypergeometric.results.sig,
             aes(x = as.factor(Pathway), y = -log10(pathways.hypergeometric.results.sig$`FDR q.val`),
                 colour = goi.count,
                 scale_colour_gradient(low="blue"),
                 size = goi.count))+
      geom_point() +
      scale_color_continuous("GOI count")+
      scale_size_continuous(range = c(5,20), guide=FALSE)+
      geom_hline(yintercept=1.30103,lty=2, color="grey") + # equivalent of p = 0.05
      geom_hline(yintercept=2,lty=4, color="grey") + # equivalent of p = 0.01
      geom_hline(yintercept=3,lty=3, color="grey") + # equivalent of p = 0.001  
      coord_flip()+
      geom_point(stat = "identity") +
      theme_bw() +
      theme(axis.text.x = element_text(colour = "black"),
            panel.grid.major = element_blank(),
            panel.grid.minor = element_blank(),
            panel.background = element_rect(fill = "white")) +
      ylim(-0.5,max.y.plot)+
      xlab("") +
      ylab("Enrichment (-log10 pvalue)")
    
    pdf("KEGG.Significant.enrichment.plot.pdf")
    print(sig.kegg.plot)
    dev.off()
  }
}